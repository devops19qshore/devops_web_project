<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        #divResult span {
            font-weight: bold;
        }

        * {
            box-sizing: border-box;
        }

        body {
            background-color: #f1f1f1;
        }

        #regForm {
            background-color: #ffffff;
            margin: 100px auto;
            font-family: Raleway;
            padding: 40px;
            width: 70%;
            min-width: 300px;
        }

        h1 {
            text-align: center;
        }

        input {
            padding: 10px;
            width: 100%;
            font-size: 17px;
            font-family: Raleway;
            border: 1px solid #aaaaaa;
        }

            /* Mark input boxes that gets an error on validation: */
            input.invalid {
                background-color: #ffdddd;
            }

        /* Hide all steps by default: */
        .tab {
            display: none;
        }

        button {
            background-color: #4CAF50;
            color: #ffffff;
            border: none;
            padding: 10px 20px;
            font-size: 17px;
            font-family: Raleway;
            cursor: pointer;
        }

            button:hover {
                opacity: 0.8;
            }

        #prevBtn {
            background-color: #bbbbbb;
        }

        /* Make circles that indicate the steps of the form: */
        .step {
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbbbbb;
            border: none;
            border-radius: 50%;
            display: inline-block;
            opacity: 0.5;
        }

            .step.active {
                opacity: 1;
            }

            /* Mark the steps that are finished and valid: */
            .step.finish {
                background-color: #4CAF50;
            }
    </style>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

    
    <link rel="stylesheet" href="https://www.jqueryscript.net/demo/Fullscreen-Loading-Modal-Indicator-Plugin-For-jQuery-loadingModal/css/jquery.loadingModal.css">
    

    <!--<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>-->
    <script type="text/javascript" src="https://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
    <script src="https://www.jqueryscript.net/demo/Fullscreen-Loading-Modal-Indicator-Plugin-For-jQuery-loadingModal/js/jquery.loadingModal.js"></script>

    <title></title>
</head>
<body>

    <form id="regForm">

        <!-- One "tab" for each step in the form: -->
        <div class="tab">
            <h1>Login:</h1>
            <p><input placeholder="Email..." oninput="this.className = ''" name="loginEmail" id="loginEmail"></p>
            <p><input placeholder="Password..." oninput="this.className = ''" name="loginPassword" id="loginPassword"></p>
        </div>

        <div class="tab">
            <h1>Services:</h1>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="ServiceRadio" id="CloudService" value="Cloud Service">
                <label class="form-check-label" for="inlineRadio1">Cloud Service</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="ServiceRadio" id="SecurityService" value="Security Service">
                <label class="form-check-label" for="inlineRadio2">Security Service</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="ServiceRadio" id="InfratructureService" value="Infrastructure Service">
                <label class="form-check-label" for="inlineRadio3">Infrastructure Service</label>
            </div>

        </div>

        <div class="tab">
            <h1>Cloud Services:</h1>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="CloudServicesRadio" id="AWS" value="AWS">
                <label class="form-check-label" for="inlineRadio1">AWS-cloud</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="CloudServicesRadio" id="Azure" value="Azure" onclick="ShowAlert('Azure');">
                <label class="form-check-label" for="inlineRadio2">Azure</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="CloudServicesRadio" id="Google" value="Google" onclick="ShowAlert('Google');">
                <label class="form-check-label" for="inlineRadio3">Google</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="CloudServicesRadio" id="OS" value="OS" onclick="ShowAlert('OS');">
                <label class="form-check-label" for="inlineRadio3">OS</label>
            </div>
        </div>

        <div class="tab">
            AWS:
            <p><input placeholder="Access key..." oninput="this.className = ''" name="AWSLoginUserID" id="AWSLoginUserID"></p>
            <p><input placeholder="Secret key..." oninput="this.className = ''" name="AWSLoginUserPassword" id="AWSLoginUserPassword" type="password"></p>
        </div>

        <div class="tab">
            <h1>After Login:</h1>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="inlineRadioCloudServices" id="AfterLoginCompute" value="Compute" checked>
                <label class="form-check-label" for="inlineRadio1">Compute</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="inlineRadioCloudServices" id="AfterLoginNetwork" value="Network">
                <label class="form-check-label" for="inlineRadio1">Network</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="inlineRadioCloudServices" id="AfterLoginStorage" value="Storage" onclick="ShowAlert('AfterLoginStorage');">
                <label class="form-check-label" for="inlineRadio2">Storage</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="inlineRadioCloudServices" id="AfterLoginSecurity" value="Security" onclick="ShowAlert('AfterLoginSecurity');">
                <label class="form-check-label" for="inlineRadio3">Security</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="inlineRadioCloudServices" id="AfterLoginOS" value="OS" onclick="ShowAlert('AfterLoginOS');">
                <label class="form-check-label" for="inlineRadio3">OS</label>
            </div>
        </div>

        <div class="tab">
            Form:
            <p><input placeholder="ami_id..." oninput="this.className = ''" name="ami_id" id="ami_id"></p>
            <p><input placeholder="subnet_pub1..." oninput="this.className = ''" name="subnet_pub1" id="subnet_pub1"></p>

            <p>
                <select required placeholder="Region to deploye compute..." oninput="this.className = ''" name="Region" id="Region" style="padding: 10px; width: 100%; font-size: 17px; font-family: Raleway;border: 1px solid #aaaaaa;">
                    <option value="us-east-2">us-east-2</option>
                    <option value="us-east-1">us-east-1</option>
                    <option value="us-west-1">us-west-1</option>
                    <option value="us-west-2">us-west-2</option>

                    <option value="ap-east-1">ap-east-1</option>
                    <option value="ap-south-1">ap-south-1</option>
                    <option value="ap-northeast-3">ap-northeast-3</option>
                    <option value="ap-northeast-2">ap-northeast-2</option>
                    <option value="ap-southeast-1">ap-southeast-1</option>
                    <option value="ap-southeast-2">ap-southeast-2</option>
                    <option value="ap-northeast-1">ap-northeast-1</option>
                    <option value="ca-central-1">ca-central-1</option>
                    <option value="cn-north-1">cn-north-1</option>
                    <option value="cn-northwest-1">cn-northwest-1</option>
                    <option value="eu-central-1">eu-central-1</option>
                    <option value="eu-west-1">eu-west-1</option>
                    <option value="eu-west-2">eu-west-2</option>
                    <option value="eu-west-3">eu-west-3</option>
                    <option value="eu-north-1">eu-north-1</option>
                    <option value="me-south-1">me-south-1</option>

                    <option value="sa-east-1">sa-east-1</option>
                    <option value="us-gov-east-1">us-gov-east-1</option>
                    <option value="us-gov-west-1">us-gov-west-1</option>
                </select>
                <!--<input placeholder="Region to deploye compute..." oninput="this.className = ''" name="Region" id="Region"></p>-->

            <p><input placeholder="Company name..." oninput="this.className = ''" name="CompanyName" id="CompanyName"></p>
            <p><input placeholder="Company Id..." oninput="this.className = ''" name="CompanyId" id="CompanyId"></p>

            <p><input placeholder="Count of VM's'..." oninput="this.className = ''" name="VM" id="VM"></p>
            <p><input placeholder="Count of RAM..." oninput="this.className = ''" name="RAM" id="RAM"></p>
            <p><input placeholder="Count of CPU..." oninput="this.className = ''" name="CPU" id="CPU"></p>
            <p><input placeholder="Count of VPC..." oninput="this.className = ''" name="VPC" id="VPC"></p>
        </div>

        <div class="tab" id="divResult">
            Result:
            <div id="DivJsonResult">


            </div>
            <!--<p>loginEmail : <span id="spnloginEmail"></span></p>
            <p>loginPassword : <span id="spnloginPassword"></span></p>

            <br />
            <p>ServiceRadio : <span id="spnServiceRadio"></span></p>

            <br />
            <p>CloudServicesRadio : <span id="spnCloudServicesRadio"></span></p>

            <br />
            <p>AWSLoginUserID : <span id="spnAWSLoginUserID"></span></p>
            <p>AWSLoginUserPassword : <span id="spnAWSLoginUserPassword"></span></p>

            <br />
            <p>AfterLoginService : <span id="spnAfterLoginService"></span></p>

            <br />
            <p>ami_id : <span id="spnami_id"></span></p>
            <p>subnet_pub1 : <span id="spnsubnet_pub1"></span></p>

            <p>CompanyName : <span id="spnCompanyName"></span></p>
            <p>CompanyId : <span id="spnCompanyId"></span></p>
            <p>Region : <span id="spnRegion"></span></p>
            <p>VM : <span id="spnVM"></span></p>
            <p>RAM : <span id="spnRAM"></span></p>
            <p>CPU : <span id="spnCPU"></span></p>
            <p>VPC : <span id="spnVPC"></span></p>-->
        </div>

        <div style="overflow:auto;">
            <div style="float:right;">
                <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
            </div>
        </div>
        <!-- Circles which indicates the steps of the form: -->
        <div style="text-align:center;margin-top:40px;">
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <!--<span class="step"></span>-->
        </div>

    </form>

    <script>
       

       

        function ShowAlert(value) {
            swal("Work in progress !", "", "error");
            $("#" + value).prop("checked", false)
        }


        function SyncFrom() {


            var loginEmail = $("#loginEmail").val();
            var loginPassword = $("#loginPassword").val();
            var ServiceRadio = $("input[name='ServiceRadio']:checked").val();
            var CloudServicesRadio = $("input[name='CloudServicesRadio']:checked").val();
            var AWSLoginUserID = $("#AWSLoginUserID").val();
            var AWSLoginUserPassword = $("#AWSLoginUserPassword").val();

            var AfterLoginService = $("input[name='inlineRadioCloudServices']:checked").val();
            //var AfterLoginNetwork = $("#AfterLoginNetwork").val();
            //var AfterLoginStorage = $("#AfterLoginStorage").val();
            //var AfterLoginSecurity = $("#AfterLoginSecurity").val();
            //var AfterLoginOS = $("#AfterLoginOS").val();

            var ami_id = $("#ami_id").val();
            var subnet_pub1 = $("#subnet_pub1").val();
            var CompanyName = $("#CompanyName").val();
            var CompanyId = $("#CompanyId").val();
            var Region = $("#Region").val();
            var VM = $("#VM").val();
            var RAM = $("#RAM").val();
            var CPU = $("#CPU").val();
            var VPC = $("#VPC").val();


            // set values
            $("#spnloginEmail").html(loginEmail);
            $("#spnloginPassword").html(loginPassword);

            $("#spnServiceRadio").html(ServiceRadio);
            $("#spnCloudServicesRadio").html(CloudServicesRadio);
            $("#spnAWSLoginUserID").html(AWSLoginUserID);
            $("#spnAWSLoginUserPassword").html(AWSLoginUserPassword);

            $("#spnAfterLoginService").html(AfterLoginService);
            //$("#spnAfterLoginNetwork").html(AfterLoginNetwork);
            //$("#spnAfterLoginStorage").html(AfterLoginStorage);
            //$("#spnAfterLoginSecurity").html(AfterLoginSecurity);
            //$("#spnAfterLoginOS").html(AfterLoginOS);

            $("#spnami_id").html(ami_id);
            $("#spnsubnet_pub1").html(subnet_pub1);

            $("#spnCompanyName").html(CompanyName);
            $("#spnCompanyId").html(CompanyId);
            $("#spnRegion").html(Region);
            $("#spnVM").html(VM);
            $("#spnRAM").html(RAM);
            $("#spnCPU").html(CPU);
            $("#spnVPC").html(VPC);
            debugger;
            var model = { "loginEmail ": loginEmail, "loginPassword ": loginPassword, "ServiceRadio ": ServiceRadio, "CloudServicesRadio ": CloudServicesRadio, "AWSLoginUserID ": AWSLoginUserID, "AWSLoginUserPassword ": AWSLoginUserPassword, "AfterLoginService ": AfterLoginService, "CompanyName ": CompanyName, "CompanyId ": CompanyId, "Region ": Region, "VM ": VM, "RAM ": RAM, "CPU ": CPU, "VPC ": VPC, "ami_id": ami_id, "subnet_pub1": subnet_pub1 };
            console.log(model);
            console.log(JSON.stringify(model));
            $("#DivJsonResult").html('');
            $("#DivJsonResult").html(JSON.stringify(model));
        }



        var currentTab = 0; // Current tab is set to be the first tab (0)
        showTab(currentTab); // Display the current tab

        function showTab(n) {
            // This function will display the specified tab of the form...
            var x = document.getElementsByClassName("tab");
            x[n].style.display = "block";
            //... and fix the Previous/Next buttons:
            if (n == 0) {
                document.getElementById("prevBtn").style.display = "none";
            } else {
                document.getElementById("prevBtn").style.display = "inline";
            }
            if (n == (x.length - 1)) {
                document.getElementById("nextBtn").innerHTML = "Submit";


            } else {
                document.getElementById("nextBtn").innerHTML = "Next";
            }
            //... and run a function that will display the correct step indicator:
            fixStepIndicator(n)
        }

        function nextPrev(n) {
            // This function will figure out which tab to display
            var x = document.getElementsByClassName("tab");
            // Exit the function if any field in the current tab is invalid:
            if (n == 1 && !validateForm()) return false;
            // Hide the current tab:
            x[currentTab].style.display = "none";
            // Increase or decrease the current tab by 1:
            currentTab = currentTab + n;
            // if you have reached the end of the form...
            if (currentTab >= x.length) {
                // ... the form gets submitted:
                //document.getElementById("regForm").submit();
                return false;
            }
            // Otherwise, display the correct tab:
            showTab(currentTab);
        }

        function validateForm() {
            // This function deals with validation of the form fields
            var x, y, i, valid = true;
            x = document.getElementsByClassName("tab");
            y = x[currentTab].getElementsByTagName("input");
            // A loop that checks every input field in the current tab:
            for (i = 0; i < y.length; i++) {
                // If a field is empty...
                if (y[i].value == "") {
                    // add an "invalid" class to the field:
                    y[i].className += " invalid";
                    // and set the current valid status to false
                    valid = false;
                }
            }
            // If the valid status is true, mark the step as finished and valid:
            if (valid) {
                //document.getElementsByClassName("step")[currentTab].className += " finish";
                SyncFrom();
            }
            return valid; // return the valid status
        }

        function fixStepIndicator(n) {
            // This function removes the "active" class of all steps...
            var i, x = document.getElementsByClassName("step");
            for (i = 0; i < x.length; i++) {
                x[i].className = x[i].className.replace(" active", "");
            }
            //... and adds the "active" class on the current step:
            x[n].className += " active";
        }

        $("#nextBtn").click(function PostData() {
            if (document.getElementById("nextBtn").innerHTML == "Submit") {
                CreateInstance();
            }
            else {

            }
        })

        function CreateInstance() {
            debugger
            $('body').loadingModal({
                text: 'Your instance is creating ...'
            });
           // swal("Your instance is creating ...", "", "error");
            var xmlhttp;

            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else if (window.ActiveXObject) {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else {
                alert("Your browser does not support XMLHTTP!");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    // hide the loading modal
                    $('body').loadingModal('hide');

                }
            }

            xmlhttp.open("POST", "http://localhost:8080/job/startec2instance/buildWithParameters?token=token1token1" + "&admin_first=" + $("#AWSLoginUserPassword").val() + "&username=" + $("#AWSLoginUserID").val() + "&region=" + $("#Region").val() + "&amiid=" + $("#ami_id").val() + "&subnetid=" + $("#subnet_pub1").val() , true);
            xmlhttp.send(null);
        }


        

    </script>
    <!--<script>
        $('body').loadingModal({
            position: 'auto',
            text: '',
            color: '#fff',
            opacity: '0.7',
            backgroundColor: 'rgb(0,0,0)',
            animation: 'doubleBounce'
        });
    </script>-->

</body>
</html>
